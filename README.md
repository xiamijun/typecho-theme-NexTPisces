# typecho主题NexTPisces修改版

## 项目介绍
NexT.Pisces主题的Typecho移植版的修改版

根据自己的需要和使用习惯，做出一些简单修改


## 安装教程

1. 上传至/usr/themes/NexTPisces目录
2. 后台启用

## 使用说明

1. 新建分类页，缩略名为categories，自定义模板选择categories
2. 新建归档页，缩略名为archive，自定义模板选择archives
3. 新建标签页，缩略名为tags，自定义模板选择tags
4. 后台配置外观选项，包括头像、昵称、站点描述、侧边栏相关设置

## 修改记录：

1.增加ICP备案设置，方便国内使用
2.使用开放CDN，加速国内访问
3.边栏头像修改为圆形(个人喜好)
4.删除一些不必要的资源文件

## 预览：
[XiaMi's Blog](https://www.wulalalala.com/)

## 鸣谢：

修改自：https://github.com/newraina/typecho-theme-NexTPisces